## Install

Vagrant: https://www.vagrantup.com/downloads.html <br />
Git (Git bash): https://git-scm.com/download/

## Configure git
1. Open git bash
2. git config --global --edit
3. Add new line:
autocrlf = input
4. Save: Esc + :wq

## Run Vm and Docker containers:

1. Open git bash
2. cd /c
3. mkdir vms
4. cd vms
5. git clone https://gitlab.com/a.khoma/laradock-magento2.git
6. cd laradock-magento2
7. vagrant up
8. ssh vagrant@127.0.0.1 -p 2222 
pwd: vagrant
9. cd /var/www/laradock
10. docker-compose up -d nginx mysql maildev
11. docker ps -a


## Create db for magento 2 project:

1. cd /var/www/laradock
2. docker-compose exec mysql bash
3. mysql -u root -p <br />
password: root
4. CREATE DATABASE magento2ce;
5. exit
6. exit



## Install magento from dev branch:
1. cd /var/www/laradock
2. docker-compose exec workspace bash
3. cd /var/www/projects
4. rm -r magento2ce
5. mkdir magento2ce
6. cd magento2ce
7. ***git url can be yours (forked m2 repository)*** <br />
git clone https://github.com/magento/magento2.git .
8. composer install
9. php bin/magento setup:install --base-url=http://magento2ce.loc/ --db-host=mysql --db-name=magento2ce --db-user=root --db-password=root --admin-firstname=Magento --admin-lastname=User --admin-email=user@example.com --admin-user=admin --admin-password=1qwerty --language=en_US --currency=USD --timezone=America/Chicago --use-rewrites=1 --backend-frontname=admin
10. Install test data (optional):  
php bin/magento setup:perf:generate-fixtures /var/www/projects/magento2ce/setup/performance-toolkit/profiles/ce/small.xml
11. php bin/magento deploy:mode:set developer
12. chmod -R 777 .
13. In windows file C:\Windows\System32\drivers\etc\hosts
add new line:
127.0.0.1   magento2ce.loc 
14. Open in browser: http://magento2ce.loc

## Setup and run unit and integration test:

1. In bash exit from container workspace run command: <br />
exit
2. cd /var/www/laradock
3. docker-compose exec mysql bash
4. mysql -u root -p <br />
password: root
5. CREATE DATABASE magento_integration_tests;
6. GRANT ALL PRIVILEGES ON \*.\* TO 'test'@'%';
7. exit
8. exit
9. Open file /var/www/projects/magento2ce/dev/tests/integration/etc/install-config-mysql.php.dist <br />
vim /var/www/projects/magento2ce/dev/tests/integration/etc/install-config-mysql.php.dist <br />
and set configuration: <br />
    'db-host' => 'mysql', <br />
    'db-user' => 'test', <br />
    'db-password' => '', <br />
    'db-name' => 'magento_integration_tests', <br />
10. cd /var/www/laradock
11. docker-compose exec workspace bash
12. ***Run unit test:*** <br />
cd /var/www/projects/magento2ce/ <br />
php -f vendor/bin/phpunit -- -c dev/tests/unit/phpunit.xml.dist ./app/code/Magento/Cms/Test/Unit/Helper/PageTest.php <br />
php -f vendor/bin/phpunit -- -c dev/tests/unit/phpunit.xml.dist ***/-path to dir or test file-/***
13. ***Run integration test:*** <br />
cd /var/www/projects/magento2ce/dev/tests/integration <br />
php -f ../../../vendor/bin/phpunit ../../../dev/tests/integration/testsuite/Magento/Cms/Block/PageTest.php <br />
php -f ../../../vendor/bin/phpunit ***/-path to dir or test file-/*** <br />

## Deployment config:
https://www.screencast.com/t/b5fYRxvLwKVu <br />
https://www.screencast.com/t/9eFlLP1ekzbc

## Debug config:
https://www.screencast.com/t/dh10yRsA <br />
https://www.screencast.com/t/ZbGpa3MQ7qF

***Configure start/stop debug buttons:*** https://www.jetbrains.com/phpstorm/marklets/

<br />
## ***More info:*** https://gitlab.com/a.khoma/laradock-magento2/
