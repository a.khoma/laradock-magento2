# Vagrant-Docker box for Magento 1 and Magento 2 development
#### Soft is based on next components:
1. Vagrant (https://www.vagrantup.com/)
2. Docker (https://www.docker.com/)
3. Laradock (http://laradock.io/)

## Install
***Vagrant:*** https://www.vagrantup.com/downloads.html <br />
***VirtualBox:*** https://www.virtualbox.org/wiki/Downloads <br />
***Git (Git bash):*** https://git-scm.com/download/

## Configure git
1. Open git bash
2. git config --global --edit
3. Add new line:
autocrlf = input
4. Save: Esc + :wq

## Tutorial
1. Open git bash
2. cd /c
3. mkdir vms
4. cd vms
5. git clone https://gitlab.com/a.khoma/laradock-magento2.git
6. cd laradock-magento2
7. vagrant up
8. ssh vagrant@127.0.0.1 -p 2222 <br />
pwd: vagrant
9. cd /var/www/laradock
10. docker-compose up -d nginx mysql maildev
11. docker ps -a

***Note:***
After each vagrant virtual machine reboot we should run containers again: <br />
docker-compose up -d nginx mysql maildev

***Open in browser:*** http://localhost <br />
(default website in /var/www/projects/public)

***Magento 2 default website:*** <br />
in file C:\Windows\System32\drivers\etc\hosts <br />
***add*** <br />
127.0.0.1	magento2ce.loc <br />
***Open in browser:*** http://magento2ce.loc <br />
(default website in /var/www/projects/magento2ce)

***Magento 1 default website:*** <br />
in file C:\Windows\System32\drivers\etc\hosts <br />
***add*** <br />
127.0.0.1	magento1ce.loc <br />
***Open in browser:*** http://magento1ce.loc <br />
(default website in /var/www/projects/magento1ce) <br />
***Note:*** for magento 1 project we should use php 5.6 (see ***Change php version*** section)

***NGINX configuration*** is located in ***folder***: ./www/laradock/nginx/

## ssh
***SSH Address:*** 127.0.0.1 <br />
***Port:*** 2222 <br />
***SSH Username:*** vagrant <br />
***SSH Password:*** vagrant

## MySql remote access
***Host:*** 127.0.0.1 <br />
***Port:*** 3306 <br />
***SSH Username:*** root <br />
***SSH Password:*** root

## MySql in application
***Host:*** mysql <br />
***Port:*** 3306 <br />
***SSH Username:*** root <br />
***SSH Password:*** root

## xdebug configuration
***IDE serverName:*** laradock <br />
***IDE key:*** PHPSTORM

### Start/Stop
1. cd /var/www/laradock
2. bash php-fpm/xdebug start
3. bash php-fpm/xdebug stop
<br />
***Configure start/stop debug buttons:*** https://www.jetbrains.com/phpstorm/marklets/

## Change php version:
1. cd /var/www/laradock
2. docker-compose down
3. in ./www/laradock/.env file set PHP_VERSION=<version> <br />
(56,70,71)
4. docker-compose build php-fpm
5. docker-compose up -d nginx mysql maildev <br />
(note: first time it will be slow, but after builds it will be taken from cache and will be fast)

## MailDev
http://localhost:1080

## Access to containers
cd /var/www/laradock <br />
docker-compose exec "container name" bash <br />
docker-compose exec mysql bash

## CLI scripts (php, composer, ...)
Should be run in container ***workspace*** <br />
1. cd /var/www/laradock <br />
2. docker-compose exec workspace bash

## Environment customization
Use file ./www/laradock/.env <br />
(file can be edited in windows/mac as folder www/laradock is ***shared***) <br />
For more info of customization please use: http://laradock.io/documentation/


## Install Magento 2
***Create db for magento 2 project:***
1. cd /var/www/laradock
2. docker-compose exec mysql bash
3. mysql -u root -p 
<br />
password: root
4. CREATE DATABASE `magento2ce`;
5. exit
6. exit
<br />

***Install magento 2 using composer:***
1. cd /var/www/laradock
2. docker-compose exec workspace bash
3. cd /var/www/projects
4. rm -r magento2ce/
5. mkdir magento2ce
6. cd magento2ce
7. composer create-project --repository-url=https://repo.magento.com/ magento/project-community-edition:2.2.1 .
8. php bin/magento setup:install --base-url=http://magento2ce.loc/ --db-host=mysql --db-name=magento2ce --db-user=root --db-password=root --admin-firstname=Magento --admin-lastname=User --admin-email=user@example.com --admin-user=admin --admin-password=1qwerty --language=en_US --currency=USD --timezone=America/Chicago --use-rewrites=1 --backend-frontname=admin
9. Install test data (optional): <br /> 
php bin/magento setup:perf:generate-fixtures /var/www/projects/magento2ce/setup/performance-toolkit/profiles/ce/small.xml
10. chmod -R 777 var pub app/etc generated
11. ***Open in browser:*** http://magento2ce.loc

## Enable elasticsearch for Magento 2
1. cd /var/www/laradock
2. docker-compose up -d elasticsearch

### Magento 2 configuration:
1. Log in to the Magento Admin as an administrator
2. Click Stores > Settings > Configuration > Catalog > Catalog > Catalog Search
<br />
***Search Engine:*** Elasticsearch <br />
***Elasticsearch Server Hostname:*** elasticsearch <br />
***Elasticsearch Server Port:*** 9200 <br />
3. Reindex data:
php ***"your Magento install dir"***/bin magento indexer:reindex catalogsearch_fulltext 
4. Refresh the full page cache in Admin



## Run Unit test in Magento 2
Tests should be run in container workspace

1. cd /var/www/laradock
2. docker-compose exec workspace bash
1. cd /var/www/projects/***magento2ce***
2. php -f vendor/bin/phpunit -- -c dev/tests/unit/phpunit.xml.dist ***"path to folder test or file"*** 
<br />
***example:***
<br />
php -f vendor/bin/phpunit -- -c dev/tests/unit/phpunit.xml.dist ./vendor/magento/module-cms/Test/Unit/
<br/>
php -f vendor/bin/phpunit -- -c dev/tests/unit/phpunit.xml.dist ./vendor/magento/module-cms/Test/Unit/Helper/PageTest.php
<br />
***More info:*** http://devdocs.magento.com/guides/v2.0/test/unit/unit_test_execution_cli.html

## Run Integration test in Magento 2
1. cd /var/www/laradock
2. docker-compose exec mysql bash
3. mysql -u root -p

password: root
4. CREATE DATABASE magento_integration_tests;
5. GRANT ALL PRIVILEGES ON *.* TO 'test'@'%';
6. exit
7. exit
8. update file magento_dir/dev/tests/integration/etc/install-config-mysql.php.dist
with values: <br />
    'db-host' => 'mysql', <br />
    'db-user' => 'test', <br />
    'db-password' => '', <br />
    'db-name' => 'magento_integration_tests'
	
9. cd /var/www/laradock
10. docker-compose exec workspace bash
11. cd /var/www/projects/magento2ce/dev/tests/integration
12. php -f ../../../vendor/bin/phpunit path_to_test <br />
***example:***<br />
php -f ../../../vendor/bin/phpunit ../../../dev/tests/integration/testsuite/Magento/Cms/Block/PageTest.php
<br />
***More info:*** http://devdocs.magento.com/guides/v2.2/test/integration/integration_test_execution.html